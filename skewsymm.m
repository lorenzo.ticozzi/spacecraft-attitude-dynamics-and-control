% -------------------------------------------------------------------------
function X = skewsymm(x)
% -------------------------------------------------------------------------

% skewsymm.m gives as output the 3x3 skew-symmetric matrix built upon the
% components of the x vector given as an input

% Components of the vector
a = x(1);
b = x(2);
c = x(3);

% Build the skew-symmetric matrix
X = [0 -c  b;
     c  0 -a;
    -b  a  0];

end