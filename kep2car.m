function [r_vec,v_vec] = kep2car(a, e, i, OM, om, theta, mu)

%--------------------------------------------------------------------------
%
% PROTOTYPE:    [r_vec,v_vec] = kep2car(a,e,i,OM,om,theta)
%
% DESCRIPTION:  kep2car.m implements the passage from keplerian elements 
%               to cartesian ones.
%
% INPUT:        a      Semi-major axis of the orbit
%               e      Eccentricity of the orbit
%               i      Inclination of the orbit wrt the celestial equator
%               OM     Right ascension of the ascending node
%               om     Anomaly of the pericenter
%               theta  True anomaly on the orbit
%
% OUTPUT:    r_vec   (3x1) Position vector in the cartesian reference frame  
%            v_vec   (3x1) Position vector in the cartesian referenca frame
%
% FUNCTIONS CALLED:        ---------
%
% AUTHOR:                 Michele Maranesi, Lorenzo Maria Ticozzi and 
%                         Salvedor Ribes, 23/07/2018, MATLAB
%   
% CHANGELOG:              ---------
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------

% R3(OM) perform the rotation around k to move i on the node line
R3_OM = [cos(OM) sin(OM) 0;
         -sin(OM) cos(OM) 0;
         0        0       1];  
  
% R1(i) performs the rotation around the node line to move x,y on the
% orbital plane
R1_i = [1     0        0;
        0  cos(i) sin(i);
        0 -sin(i) cos(i)];

% R3(om) performs the rotation to move the node line on the direction of
% the eccentricity
R3_om = [cos(om)  sin(om)  0;
         -sin(om) cos(om)  0;
         0        0        1];

% Rotation of the vectors via rotation matrices

R_car2kep = R3_om * R1_i * R3_OM;
R_kep2car = R_car2kep';

p = a * (1-e^2);
r_kep = [p*cos(theta)/(1+e*cos(theta)); % Radius in the keplerian reference
         p*sin(theta)/(1+e*cos(theta));
         0];                           

v_kep = [sqrt(mu/p)* (-sin(theta));      % Vel in the keplerian reference
         sqrt(mu/p)* (e + cos(theta));
                                  0];

r_vec = R_kep2car * r_kep;
v_vec = R_kep2car * v_kep;


return



